package com.springbatch.config;

import com.springbatch.fieldmapper.BankFieldSetMapper;
import com.springbatch.model.Bank;
import com.springbatch.processor.BankItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.ResourceAwareItemReaderItemStream;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;

@Configuration
public class MultipleSourceFileToMySqlBatchJobConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;

    @Value("classpath*:/data/bank-record*.txt")
    private Resource[] inputFiles;

    @Bean
    public MultiResourceItemReader<Bank> multiResourceItemReader() {
        MultiResourceItemReader<Bank> reader = new MultiResourceItemReader<>();
        reader.setDelegate(bankItemReader());
        reader.setResources(inputFiles);
        return reader;
    }

    @Bean
    public FlatFileItemReader<Bank> bankItemReader() {
        FlatFileItemReader<Bank> reader = new FlatFileItemReader<>();
        DefaultLineMapper<Bank> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(new String[]{"id","bank","branch","ifsc","contact","address","city","district","state"});
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setFieldSetMapper(new BankFieldSetMapper());
        lineMapper.afterPropertiesSet();

        reader.setLineMapper(lineMapper);
        return reader;

    }

     @Bean
    public JdbcBatchItemWriter<Bank> itemWriter() {
         JdbcBatchItemWriter<Bank> writer = new JdbcBatchItemWriter<>();
         writer.setDataSource(dataSource);
         writer.setSql("INSERT INTO BANK_INSERT VALUES (:id, :bank, :branch, :ifsc, :contact, :address, :city, :district, :state)");
         writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
         writer.afterPropertiesSet();
         return writer;
     }

     @Bean
    public Step step1() {
        return stepBuilderFactory.get("importMultipleSourceFileDataToDBStep1")
                .<Bank,Bank>chunk(80)
                .reader(multiResourceItemReader())
                .writer(itemWriter())
                .build();
     }

    @Bean
    public Job importMultipleSourceFileDataToDB() {
        return jobBuilderFactory.get("importMultipleSourceFileDataToDB")
                .start(step1())
                .build();
    }


}
