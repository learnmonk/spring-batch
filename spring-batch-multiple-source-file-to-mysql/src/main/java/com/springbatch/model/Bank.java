package com.springbatch.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bank {
    private long id;
    private String bank;
    private String ifsc;
    private String branch;
    private String address;
    private String contact;
    private String city;
    private String district;
    private String state;

}
