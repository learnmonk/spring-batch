package com.springbatch.processor;

import com.springbatch.model.Bank;
import org.springframework.batch.item.ItemProcessor;

public class BankItemProcessor implements ItemProcessor<Bank,Bank> {

    @Override
    public Bank process(Bank item) throws Exception {
        return item;
    }
}
