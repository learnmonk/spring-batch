package com.springbatch.processor;
import com.springbatch.model.Bank;
import com.springbatch.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class BankProcessor implements ItemProcessor<Bank, Bank> {

  private String threadName;

  public String getThreadName() {
    return threadName;
  }

  public void setThreadName(String threadName) {
    this.threadName = threadName;
  }

  @Override
  public Bank process(Bank item) throws Exception {
//    System.out.println(threadName + " processing : "
//        + item.getId() + " : " + item.getUsername());
    return item;
  }
}
