package com.springbatch.model;

import lombok.Data;

@Data
public class User {

  int id;
  String username;
  String password;
  int age;
}
