package com.springbatch.config;

import com.springbatch.fieldmapper.BankFieldSetMapper;
import com.springbatch.model.Bank;
import com.springbatch.processor.BankItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
public class BatchJobConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;

    @Bean
    public FlatFileItemReader<Bank> itemReader() {
        FlatFileItemReader<Bank> itemReader = new FlatFileItemReader<>();
        itemReader.setLinesToSkip(1);
        itemReader.setResource(new ClassPathResource("/data/banks-10lacs-record.txt"));

        DefaultLineMapper<Bank> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"id","bank","branch","ifsc","contact","address","city","district","state"});
        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(new BankFieldSetMapper());
        lineMapper.afterPropertiesSet();

        itemReader.setLineMapper(lineMapper);
        return itemReader;
    }

    @Bean
    public BankItemProcessor itemProcessor() {
        return new BankItemProcessor();
    }

     @Bean
    public JdbcBatchItemWriter<Bank> itemWriter() {
         JdbcBatchItemWriter<Bank> writer = new JdbcBatchItemWriter<>();
         writer.setDataSource(dataSource);
         writer.setSql("INSERT INTO BANK_INSERT VALUES (:id, :bank, :branch, :ifsc, :contact, :address, :city, :district, :state)");
         writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
         writer.afterPropertiesSet();
         return writer;
     }

     @Bean
    public Step step1() {
        return stepBuilderFactory.get("importBanksDataToDBStep1")
                .<Bank,Bank>chunk(100)
                .reader(itemReader())
                .processor(itemProcessor())
                .writer(itemWriter())
                .build();
     }

    @Bean
    public Job importBanksDataToDB() {
        return jobBuilderFactory.get("importBanksDataToDB")
                .start(step1())
                .build();
    }


}
