package com.springbatch.rowmapper;

import com.springbatch.model.Bank;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BankRowMapper implements RowMapper<Bank> {
    @Override
    public Bank mapRow(ResultSet resultSet, int i) throws SQLException {
        return Bank.builder()
                .id(resultSet.getLong("id"))
                .bank(resultSet.getString("bank"))
                .branch(resultSet.getString("branch"))
                .ifsc(resultSet.getString("ifsc"))
                .contact(resultSet.getString("contact"))
                .address(resultSet.getString("address"))
                .city(resultSet.getString("city"))
                .district(resultSet.getString("district"))
                .state(resultSet.getString("state"))
                .build();
    }
}
