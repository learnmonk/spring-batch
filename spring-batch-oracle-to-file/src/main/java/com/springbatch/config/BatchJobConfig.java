package com.springbatch.config;

import com.springbatch.model.Bank;
import com.springbatch.processor.BankItemProcessor;
import com.springbatch.rowmapper.BankRowMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
public class BatchJobConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;

    @Bean
    public JdbcCursorItemReader<Bank> itemReader() {
        JdbcCursorItemReader<Bank> itemReader = new JdbcCursorItemReader<>();
        itemReader.setSql("SELECT id, bank,branch,ifsc,contact,address,city,district,state from bank_insert order by id");
        itemReader.setDataSource(this.dataSource);
        itemReader.setRowMapper(new BankRowMapper());
        return itemReader;
    }

    @Bean
    public BankItemProcessor itemProcessor() {
        return new BankItemProcessor();
    }

     @Bean
    public ItemWriter<Bank> itemWriter() {
         FlatFileItemWriter<Bank> itemWriter = new FlatFileItemWriter<>();
         itemWriter.setResource(new ClassPathResource("banks.txt"));
         DelimitedLineAggregator<Bank> lineAggregator = new DelimitedLineAggregator<>();
         lineAggregator.setDelimiter(",");
         BeanWrapperFieldExtractor<Bank> fieldExtractor = new BeanWrapperFieldExtractor<>();
         fieldExtractor.setNames(new String[]{"id","bank","branch","ifsc","contact","address","city","district","state"});
         lineAggregator.setFieldExtractor(fieldExtractor);
         itemWriter.setLineAggregator(lineAggregator);
         return itemWriter;
     }

     @Bean
    public Step step1() {
        return stepBuilderFactory.get("step4")
                .<Bank,Bank>chunk(100)
                .reader(itemReader())
                .processor(itemProcessor())
                .writer(itemWriter())
                .build();
     }

    @Bean
    public Job exportBanksDataToFile() {
        return jobBuilderFactory.get("exportBanksDataToFile")
                .start(step1())
                .build();
    }


}
