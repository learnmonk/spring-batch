package io.spring.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworldBatchApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(HelloworldBatchApplication.class, args);
	}

}
