package com.springbatch.fieldmapper;

import com.springbatch.model.Bank;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class BankFieldSetMapper implements FieldSetMapper<Bank> {
    @Override
    public Bank mapFieldSet(FieldSet fieldSet) throws BindException {

        return Bank.builder()
                .id(fieldSet.readLong("id"))
                .bank(fieldSet.readString("bank"))
                .branch(fieldSet.readString("branch"))
                .ifsc(fieldSet.readString("ifsc"))
                .contact(fieldSet.readString("contact"))
                .address(fieldSet.readString("address"))
                .city(fieldSet.readString("city"))
                .district(fieldSet.readString("district"))
                .state(fieldSet.readString("state"))
                .build();
    }
}
