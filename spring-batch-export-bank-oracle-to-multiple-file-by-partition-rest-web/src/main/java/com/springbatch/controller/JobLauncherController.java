package com.springbatch.controller;

import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobLauncherController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobOperator jobOperator;

    @Autowired
    private Job job;

    @SneakyThrows
    @GetMapping("/job-launch")
    public ResponseEntity<String> launchJob() {
        JobParameters jobParameters = new JobParametersBuilder().addString("job","partionTest").toJobParameters();
        //JobExecution jobExecution = this.jobLauncher.run(job, jobParameters);
        Long id= this.jobOperator.start("job",String.format("name=%s","byJobOperator"));
        return ResponseEntity.ok(String.format("Job completed with jobExecutionId=%s",id));
        //return ResponseEntity.ok(String.format("Job Started at: %s, ended at: %s",jobExecution.getStartTime(),jobExecution.getEndTime()));
    }
}
